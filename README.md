# Berkshire Hathaway Shareholders Letters


# Análisis de las Cartas Anuales a los Accionistas de Berkshire Hathaway Inc., Escritas por Warren Buffett

## Descripción del Proyecto
Este proyecto tiene como objetivo analizar el contenido de las cartas anuales escritas por Warren Buffett, CEO de Berkshire Hathaway Inc., para evaluar los temas recurrentes y el sentimiento a lo largo del tiempo en relación con la economía y el mercado de valores. A través del procesamiento de lenguaje natural (NLP), se identifican las palabras y conceptos más utilizados y cómo han evolucionado con el tiempo.

Las cartas, dirigidas a los accionistas, reflejan la perspectiva única de Buffett sobre inversión y gestión empresarial, proporcionando insights valiosos para entender cómo se percibían los cambios económicos en diferentes épocas.

## Estructura del Proyecto
- **Exploración de Datos:** Recopilación de cartas anuales de 1977 a 2023 en distintos formatos (HTML y PDF) de acuerdo con las fechas de publicación.
- **Análisis de Texto:** Se utilizan técnicas de NLP para identificar términos clave y realizar un análisis de sentimientos en las cartas.
- **Visualización:** Se incluyen gráficos como nubes de palabras y análisis comparativo entre el sentimiento de las cartas y el desempeño de las acciones de Berkshire Hathaway y del índice S&P 500.
  
## Metodología
1. **Limpieza de Datos:** Normalización y preparación de los textos para el análisis.
2. **Análisis de Palabras Clave:** Cálculo de TF-IDF para destacar la relevancia temporal de ciertos términos.
3. **Análisis de Sentimientos:** Evaluación de la actitud de Buffett en cada carta, asignando una puntuación de sentimiento en una escala de negativo a positivo.
4. **Comparación con el Mercado:** Correlación entre el sentimiento expresado en las cartas y el comportamiento de las acciones de Berkshire Hathaway y el S&P 500.

## Resultados Destacados
- Las palabras clave más comunes incluyen términos como "negocios", "valor", "mercado", "inversión", y "costo".
- A lo largo del tiempo, la relevancia de ciertos términos ha cambiado. Por ejemplo, "million" era común en las primeras cartas, mientras que "billion" se vuelve prominente en años recientes.
- No se encontró una correlación significativa entre el sentimiento de las cartas y el desempeño de las acciones, lo cual sugiere que Buffett evita influir en el mercado con su comunicación.

## Conclusión
El análisis sugiere que las cartas de Buffett ofrecen una perspectiva valiosa, pero el sentimiento expresado no tiene un impacto directo en el precio de las acciones de Berkshire Hathaway. Para futuras investigaciones, se recomienda realizar un análisis comparativo con las cartas de otros directores ejecutivos para obtener un panorama más amplio sobre el sentimiento económico y corporativo.

## Uso del Código
Este proyecto fue realizado únicamente con fines académicos. Los textos de las cartas de Berkshire Hathaway tienen derechos de autor y están disponibles públicamente en el sitio oficial: [Cartas Anuales de Berkshire Hathaway](https://www.berkshirehathaway.com/letters/letters.html).

## Requisitos
- Python 3.x
- Bibliotecas:
  - `pandas`
  - `numpy`
  - `nltk`
  - `matplotlib`
  - `seaborn`

## Ejecución
1. Clonar el repositorio.
2. Instalar las dependencias.
3. Ejecutar el archivo principal para generar las visualizaciones y análisis de las cartas.

## Créditos
Proyecto realizado por Luis Alberto Palacios
Maestría en Prospectiva Estratégica, Escuela de Gobierno y Transformación Pública, Tecnológico de Monterrey.

---

Este análisis es un recurso académico y no tiene intenciones comerciales. El contenido se basa en un análisis académico y debe interpretarse como tal.
